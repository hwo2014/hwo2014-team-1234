-finalizing optimal lane logic

-collecting curve entry speed and collections stats from all cars 
 	=basically, maintaining max successful friction and min failure friction values
-estimating the optimal curve entry speed based on the stats
	=max successfull friction should be "safe bet", min failure friction should be "sure fail"

-estimating cars (negative) acceleration when no throttle is applied
-based on no-throttle-acceleration, estimating when to start slowing down to curves
	=should probably just calculate when we need to cease throttling
	
which lane is right/left -issue

-DONE: solving "ugly fix" (speed measurement) issue properly 
[JJA:] fixed on lane fix

-overtaking logic (this is quite hardcore)