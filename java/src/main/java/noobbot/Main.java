package noobbot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;

public class Main {
    final static boolean ONLINEMODE = true;
	public static void main(String... args) throws IOException {
    	String host;
        int port;
        String botName; 
        String botKey;
    	if (args.length<1) {
    		host = "testserver.helloworldopen.com";
    		port = 8091;
        	botName = "KolloHentti";
        	botKey = "XtQrhvAK0gPCxg";
        }
        else {
	    	host = args[0];
	        port = Integer.parseInt(args[1]);
	        botName = args[2]; 
	        botKey = args[3];
        }
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        final PrintWriter writer;
        final BufferedReader reader;
        if (ONLINEMODE) {
        	final Socket socket = new Socket(host, port);
        	writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        }
        else {
        	writer = new PrintWriter(System.out);
        	reader = new BufferedReader(new FileReader("test.txt"));
        }
        if (ONLINEMODE){
        	new Main(reader, writer, new Join(botName, botKey));
        }
        else {
        	new Main(reader, writer, null);
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private Car HenttiCar;
    
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
    	List<Piece> list = new ArrayList<Piece>();
    	List<Car> carList = new ArrayList<Car>();
        
    	if (ONLINEMODE) {
        	send(join);
        }
    	JsonParser parser = new JsonParser();
        boolean first = true;
        int nextPieceIndex = 0;
        Piece currentPiece = null;
        Piece nextPiece = null;
        HenttiCar hentti = new HenttiCar();
        hentti.throttle = 0.6f;
        while((line = reader.readLine()) != null) {
        	System.out.println(line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	carList.clear();
            	JsonArray dataArray = parser.parse(line).getAsJsonObject().getAsJsonArray("data");
            	for (JsonElement je : dataArray)
            	{
            		Car car = new Gson().fromJson(je, Car.class);
            	    carList.add(car);
            	    if (car.id.name.equals("KolloHentti")){
            	    	hentti.setAngle(car.angle);
            	    	hentti.lap = car.lap;
            	    	hentti.piecePosition = car.piecePosition;
            	    	hentti.Update();
            	    	//System.out.println(line);
            	    }
            	}   
            	currentPiece = list.get(hentti.piecePosition.pieceIndex);
            	nextPieceIndex = hentti.piecePosition.pieceIndex+1;
            	if (nextPieceIndex>list.size()-1) {
            		nextPieceIndex = 0; 
            	}
            	nextPiece = list.get(nextPieceIndex);
            	
            	System.out.println(hentti.toString()+", PIECE ANGLE:"+currentPiece.angle);
            	
            	if (!currentPiece.isStraight()) {
            		// MUTKA
            		if (hentti.piecePosition.inPieceDistance>currentPiece.length*0.5) {
            			if (hentti.throttle<0.8) hentti.throttle += 0.01;
            		}
            		else if (Math.abs(hentti.angle)<8) {
            			hentti.throttle += 0.05;
            		}
            		else {
            			hentti.throttle = (float)Math.max(0.05, 0.40-Math.abs(hentti.angle)/50);
            		}
            		
            	}
            	else {
	            	// SUORA
            		if (nextPiece.isStraight()) {
            			if (hentti.throttle<1.0f) hentti.throttle+=0.2f;
            		}
            		else {
            			hentti.throttle = 1.0f - (hentti.piecePosition.inPieceDistance / currentPiece.length) * (Math.abs(nextPiece.angle)/50);
            		}
            	}
            	if (ONLINEMODE) {
            		if (hentti.getNextCurveInLane()!=hentti.toLane) {
            			hentti.toLane = hentti.getNextCurveInLane();
            			send(new ChangeLane(hentti.toLane));
            			System.out.println("Lane change sent");
            		}
            		else send (new Throttle(hentti.throttle));
            	}
            	
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                JsonArray dataArray = parser.parse(line).getAsJsonObject().get("data")
                .getAsJsonObject().get("race").getAsJsonObject()
                .getAsJsonObject().get("track").getAsJsonObject().getAsJsonArray("pieces");
                
            	for (JsonElement je : dataArray)
            	{
            		Piece piece = new Gson().fromJson(je, Piece.class);
            	    if (piece.angle!=0) {
            	    	piece.length = (float)(piece.radius*2 * Math.PI * (piece.angle/360));
            	    }
            		list.add(piece);
            	    
            	}
            	hentti.trackPieces = list;
            	System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                if (ONLINEMODE) {
                	send(new Ping());
                }
            }
            try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class CarId
{
	public String name;
	public String color;
}

class Lane
{
	public byte startLaneIndex;
	public byte endLaneIndex;
}

class PiecePosition
{
	public int pieceIndex;
	public float inPieceDistance;
	public Lane lane;
	
}

class Car
{
	public CarId id;
	public float angle;
	public byte lap;
	public PiecePosition piecePosition;
}

class HenttiCar extends Car
{
	@Override
	public String toString() {
		return "HenttiCar [throttle=" + throttle + ", currentSpeed="
				+ currentSpeed + ", angleChange=" + angleChange + ", angle="
				+ angle + ", lap=" + lap + "]";
	}

	private PiecePosition lastPiecePosition;
	public List<Piece> trackPieces;
	public float throttle;
	
	public float currentSpeed;
	public float angleChange;
	public String toLane = "";
	
	public void setAngle(float newAngle) {
		angleChange = newAngle-this.angle;
		angle = newAngle;
	}
	
	public String getNextCurveInLane()
	{
		int i = piecePosition.pieceIndex+1;
		if (trackPieces.size()-1<i) { i = 0; }
		String inLaneCurve = null;
		while(inLaneCurve == null) {
			if (!trackPieces.get(i).isStraight()) {
				inLaneCurve = trackPieces.get(i).angle<0?"Left":"Right";
				break;
			}
			i++;
			if (trackPieces.size()-1<i) { i = 0; }
		}
		return inLaneCurve;
	}
	public void Update() {
		
		if (lastPiecePosition == null) {
			currentSpeed = piecePosition.inPieceDistance;
		}
		else if (lastPiecePosition.pieceIndex == piecePosition.pieceIndex) {
			currentSpeed = piecePosition.inPieceDistance - lastPiecePosition.inPieceDistance; 
		}
		else {
			currentSpeed = trackPieces.get(lastPiecePosition.pieceIndex).length-lastPiecePosition.inPieceDistance + piecePosition.inPieceDistance;
		}
		lastPiecePosition = piecePosition;
	}
	
	
}

class Piece
{
	public float length;
	public boolean _switch;
	public float radius;
	public float angle;
	
	public boolean isStraight() {
		return angle == 0;
	}

}

class Track
{
	public String id;
	public String name;
	public Piece[] pieces;
}

class RaceInfo
{
	Track track;
}

class RaceData
{

	RaceInfo race;
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = Math.min(1.0,value);
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class ChangeLane extends SendMsg {
    private String Lane;

    public ChangeLane(String value) {
        this.Lane = value;
    }

    @Override
    protected Object msgData() {
        return Lane;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
