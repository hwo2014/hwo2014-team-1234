package fi.kollohentti.bot;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class HenttiCar extends Car {

	private PiecePosition lastPiecePosition;
	private Piece lastPiece;
	public Piece piece;

	public boolean changingLanes = false;
	
	public double throttle;
	public double currentSpeed = 0.0;
	public double lastSpeed = 0.0;
	public double currentAcceleration = 0.0;
	public double lastAcceleration = 0.0;
	public double currentJerk = 0.0;
	public double currentJerkFactor = 0.0;
	public int currentTick = 0;
	
	public double angleChange;
	public Lane currentLane;
	public boolean isOut = false;
	public TurboAvailable turbo = null;
	public boolean useTurbo = false;

	// hypotenuse is the hypotenuse between car dimensions and guideflag
	// position
	public double hypotenuse;

	// hypotenuse angle is the angle calculated from cars back corner, always >
	// drift angle
	public double hypotenuseAngle;

	// tangent angle 90 - hypotenuseAngle
	public double maxAngle;

	// Throttle Force is
	// calculated in a following way: if throttle is 1.0 and speed raises from
	// 4.5 -> 4.7 then F is 0.2 / 1.0 = 0.2 (speedChange/throttle)
	public double throttleForce = 0.0;

	// assumption: frictionForce is same for the whole track.
	// It is calculated F = Acceleration - ThrottleForce.
	public double frictionForce;

	// this is calculated sqrt ( radius ), since gravity if constant and
	// friction u is unknown
	public double maxNextAngleSpeed = 0.0;
	
	// start with this algo:
	private Algorithm collectorAlgo = new DummyThrottle(this);
	
	private Algorithm basicAlgo = new SampleAlgorithm(this);
	
	
	public Analyzer analyzer = new Analyzer();
	
	public String toLane = "";
	public String name;
	public String color;
	
	public HenttiCar() {
	}
	
	@Override
	public String toString() {
		return "HenttiCar [pce=" + piecePosition.pieceIndex
				+ (piece.isStraight()? " STRAIGHT " : " CURVE ")
				+ ", thr=" + throttle 
				+ ", curSpd=" + currentSpeed
				+ ", curAcl=" + currentAcceleration
				+ ", curJerk=" + currentJerk
				+ ", curJerkFtr=" + currentJerkFactor
				+ ", disToCurve=" + getDistanceToNextCurve()
				+ ", angleCh=" + angleChange 
				+ ", curLane=" + currentLane.index 
				+ ", nextCurveInlane=" + getNextCurveInLane() 
				+ ", lap=" + piecePosition.lap
				+ ", inPceDist=" + piecePosition.inPieceDistance + "]";
	}
	
	public String toCSV() {
		return piecePosition.pieceIndex + ","
				+ (piece.isStraight()? "STRAIGHT," : "CURVE,")
				+ throttle + "," 
				+ currentSpeed + ","
				+ currentAcceleration + ","
				+ currentJerk + ","
				+ currentJerkFactor + ","
				+ getDistanceToNextCurve() + ","
				+ angleChange + "," 
				+ currentLane.index + "," 
				+ getNextCurveInLane() + "," 
				+ piecePosition.lap + ","
				+ piecePosition.inPieceDistance;
	}

	public boolean isOut() {
		return isOut;
	}

	public double getMaxAngleSpeed(double radius, double coefficientFriction) {
		return Math.sqrt(Math.abs(radius) * coefficientFriction);
	}

	public void updateThrottleForce() {
		this.throttleForce = this.currentAcceleration / this.throttle;
	}

	public void updateFrictionForce() {
		this.frictionForce = this.currentAcceleration - this.throttleForce;
	}

	public void setAngle(double newAngle) {
		angleChange = newAngle - this.angle;
		angle = newAngle;
	}
	
	private void updateSpeed() {
		//System.out.println(angleChange);
		lastSpeed = currentSpeed;
		lastAcceleration = currentAcceleration;
		//if (piece!=null) System.out.println(toString());
		//System.out
		//		.println("lastPos PieceIndex:" + lastPiecePosition.pieceIndex);

		if (lastPiecePosition.pieceIndex == piecePosition.pieceIndex) {
			// stayed in same piece
			currentSpeed = piecePosition.inPieceDistance
					- lastPiecePosition.inPieceDistance;
		
			Piece lastPiece = Main.racetrack.pieces.get(piecePosition.pieceIndex);
			if (lastPiece.latestEnterAngles.get(id.name) == null) {
				lastPiece.latestEnterAngles.put(id.name, new Hashtable<Integer, Double>());
			}
			if (lastPiece.latestEnterAngles.containsKey(id.name)) {
				if (!lastPiece.latestEnterAngles.get(id.name).containsKey(piecePosition.lane.endLaneIndex)) {
					lastPiece.latestEnterAngles.get(id.name).put(piecePosition.lane.endLaneIndex,Math.abs(angle));
				}
				else
				{//if (lastPiece.latestEnterAngles.get(id.name).get(piecePosition.lane.endLaneIndex)<Math.abs(angle)) {
					double lastVal = lastPiece.latestEnterAngles.get(id.name).get(piecePosition.lane.endLaneIndex);
					lastPiece.latestEnterAngles.get(id.name).replace(piecePosition.lane.endLaneIndex,Math.max(Math.abs(angle),lastVal));
				}
			}
			
		} else {
			// changed to new piece
			if (piece!=null) System.out.println("piece: "+piecePosition.pieceIndex+" STRAIGHT:"+Main.racetrack.pieces.get(piecePosition.pieceIndex).isStraight());
			int lastPieceLaneIndex = lastPiecePosition.lane.endLaneIndex;
			int pieceLaneIndex = piecePosition.lane.endLaneIndex;
			Hashtable<Integer, Double> enterSpeeds = Main.racetrack.pieces
					.get(piecePosition.pieceIndex).latestEnterSpeeds
					.get(id.name);
			if (enterSpeeds == null) {
				Main.racetrack.pieces.get(piecePosition.pieceIndex).latestEnterSpeeds
						.put(id.name, new Hashtable<Integer, Double>());
			}
			Main.racetrack.pieces.get(piecePosition.pieceIndex).latestEnterSpeeds
			.get(id.name).put(piecePosition.lane.endLaneIndex,
					currentSpeed);
			//System.out.println("ADDING "+ id.name + " "+currentSpeed);
			
			Hashtable<Integer, Double> enterAngles = Main.racetrack.pieces
					.get(piecePosition.pieceIndex).latestEnterAngles
					.get(id.name);
			if (enterAngles == null) {
				Main.racetrack.pieces.get(piecePosition.pieceIndex).latestEnterAngles
						.put(id.name, new Hashtable<Integer, Double>());
			}
			Main.racetrack.pieces.get(piecePosition.pieceIndex).latestEnterAngles
			.get(id.name).put(piecePosition.lane.endLaneIndex,
					angle);
			
			// carExited uses lane radius right
			//lastPiece.carExited(id.name, lastPieceLaneIndex, lastPiece.radius);
			lastPiece.carExited(id.name, lastPieceLaneIndex, this.currentSpeed);
			
			// double distTraveledInLastPiece = lastPiece.getCenterLineLength()
			double distTraveledInLastPiece = lastPiece
					.getLength(lastPieceLaneIndex)
					- lastPiecePosition.inPieceDistance;

			currentSpeed = distTraveledInLastPiece
					+ piecePosition.inPieceDistance;

		}

		// update acceleration and jerk
		currentAcceleration = currentSpeed - lastSpeed;
		currentJerk = currentAcceleration - lastAcceleration;
		currentJerkFactor = lastAcceleration / currentAcceleration;
		if (currentAcceleration < 0) { // when decelerating, turn ratio around
			currentJerkFactor = 1 / currentJerkFactor;
		}
		
		Piece currentPiece = Main.racetrack.pieces.get(piecePosition.pieceIndex);
		if (currentPiece.latestEnterAngles.get(id.name)==null) {
			Hashtable<Integer, Double> initial = new Hashtable<Integer, Double>();
			initial.put(currentPiece.optimalLane, 0d);
			currentPiece.latestEnterAngles.put(id.name, initial);
		}
		if (currentPiece.latestEnterAngles.get(id.name).get(currentPiece.optimalLane) == null) {
			Hashtable<Integer, Double> initial = currentPiece.latestEnterAngles.get(id.name);
			initial.put(currentPiece.optimalLane, 0d);
		}
		
		if (currentPiece.latestEnterAngles.get(id.name).get(currentPiece.optimalLane)<angleChange) {
    		currentPiece.latestEnterAngles.get(id.name).replace(currentPiece.optimalLane, angleChange);
    	}
	}

	public void Update() {
		
		if (lastPiecePosition == null) {
			lastPiecePosition = piecePosition;
		}
		updateSpeed();
		//updateFriction();
		
		updateThrottleForce();
		updateFrictionForce();
		
		// TODO: MAGIC 400 DISTANCE, CALCULATE FROM REAL DATA (Turbo ticks,
		// factor etc)
		// MOVE THIS TO ALGO THIS IS ACTUAL LOGIC NOT UPDATING DATA
		if (getDistanceToNextCurve() > 350 && turbo != null && !turbo.usingTurbo && Math.abs(angleChange)<0.35) {
			useTurbo = true;
			turbo.usingTurbo = true;
			turbo.startTick = currentTick;
			//System.out.println("USE TURBO, "+turbo.startTick+"  "+(turbo.startTick+turbo.turboDurationTicks)+"  "+currentTick);
		}
		if (turbo!=null && turbo.usingTurbo && (turbo.startTick+turbo.turboDurationTicks)<=currentTick) {
			//System.out.println("TURBO TO NULL!");
			//System.out.println("CLOSING TURBO, "+turbo.startTick+"  "+currentTick);
			turbo = null;
		}
		
		Algorithm currentAlgo = collectorAlgo;
		do {
			currentAlgo.update();
			AlgorithmType nextAlgoType = currentAlgo.getNextAlgorithm();
			if (nextAlgoType != null) {
				switch (nextAlgoType) {
				case BASIC_THROTTLE:
					currentAlgo = basicAlgo;
					break;
				case COLLECTOR:
					currentAlgo = collectorAlgo;
					break;
				default:
					currentAlgo = null;
					break;
				}
			}
			else {
				currentAlgo = null;
			}
		} 
		while (currentAlgo!= null);
		


		lastPiecePosition = piecePosition;
		lastPiece = Main.racetrack.pieces.get(lastPiecePosition.pieceIndex);
	}

	public String getNextCurveInLane() {
		int i = piecePosition.pieceIndex + 1;
		if (Main.racetrack.pieces.size() - 1 < i) {
			i = 0;
		}
		String inLaneCurve = null;
		while (inLaneCurve == null) {
			if (!Main.racetrack.pieces.get(i).isStraight()) {
				inLaneCurve = Main.racetrack.pieces.get(i).angle < 0 ? "Left"
						: "Right";
				break;
			}
			i++;
			if (Main.racetrack.pieces.size() - 1 < i) {
				i = 0;
			}
		}
		return inLaneCurve;
	}

	public float getDistanceToNextCurve() {
		Piece currentPiece = Main.racetrack.pieces
				.get(piecePosition.pieceIndex);
		
		if (!currentPiece.isStraight()) {
			return 0;
		}
		
		float dist = (float) (currentPiece.getLength(currentLane.index) - piecePosition.inPieceDistance);
		int i = piecePosition.pieceIndex + 1;
		if (Main.racetrack.pieces.size() - 1 < i) {
			i = 0;
		}

		Piece nextPiece = Main.racetrack.pieces.get(i);
		while (nextPiece.isStraight()) {
			if (Main.racetrack.pieces.get(i).isStraight()) {
				dist += Main.racetrack.pieces.get(i).getLength(
						currentLane.index);
				i++;
				if (Main.racetrack.pieces.size() - 1 < i) {
					i = 0;
				}
				nextPiece = Main.racetrack.pieces.get(i);
			}
		}
		return dist;

	}
	
	public static class CurveInfo {
		public Piece piece;
		public float distanceTo;
	}
	
	public CurveInfo[] getDistancesToNextCurves(int n) {
		Piece currentPiece = Main.racetrack.pieces.get(piecePosition.pieceIndex);
		int pieceCount = Main.racetrack.pieces.size();
		int curveCount = 0;
		CurveInfo[] ret = new CurveInfo[n];
		int nextPieceIndex = (piecePosition.pieceIndex + 1) % pieceCount;
		Piece nextPiece = Main.racetrack.pieces.get(nextPieceIndex);
		float dist = (float) (currentPiece.getLength(currentLane.index) - piecePosition.inPieceDistance);
		/*if (!currentPiece.isStraight() && nextPiece.isStraight()) {
			dist*= 2; 
		}*/
		while (curveCount < n) {
			if (!nextPiece.isStraight()) { 
				// next is curve, add current dist to ret
				CurveInfo ci = new CurveInfo();
				ci.piece = nextPiece;
				ci.distanceTo = dist;
				ret[curveCount++] = ci;
			}
			dist += nextPiece.getLength(nextPiece.optimalLane);
			++nextPieceIndex;
			nextPieceIndex %= pieceCount;
			nextPiece = Main.racetrack.pieces.get(nextPieceIndex);
		}
		return ret;
	}

	public float getDistanceToPiece(int pieceIndex) {
		Piece currentPiece = Main.racetrack.pieces
				.get(piecePosition.pieceIndex);
		float dist = (float) (currentPiece.getLength(currentLane.index) - piecePosition.inPieceDistance);
		int i = piecePosition.pieceIndex + 1;
		if (Main.racetrack.pieces.size() - 1 < i) {
			i = 0;
		}

		while (i!=pieceIndex) {
			dist += Main.racetrack.pieces.get(i).getLength(
					Main.racetrack.pieces.get(i).optimalLane);
			i++;
			if (Main.racetrack.pieces.size() - 1 < i) {
				i = 0;
			}
		}
		return dist;
	}

	
	// formula to calculate distance which is needed to slow down from
	// speedNow to wantedSpeed ( e.g curve max entry speed ) with current
	// acceleration
	public double getSlowDownDistance(double speedNow, double wantedSpeed,
			double acceleration) {

		return (Math.pow(wantedSpeed, 2) - Math.pow(speedNow, 2))
				/ (2 * acceleration);
	}

	private static double logOfBase(double base, double num) {
		double temp = Math.log(num) / Math.log(base);
		return temp;
	}

	public double getPredictedTime(double speedChange, double curAcceleration,
			double jerkFactor) {
		return logOfBase(jerkFactor, (speedChange * Math.log(jerkFactor)
				/ curAcceleration + 1));
	}

	public double getSlowDownDistance(double curAccel, double curSpeed,
			double jerkFactor, double delay) {
		return distanceIntegral(curAccel, curSpeed, jerkFactor, delay)
				- distanceIntegral(curAccel, curSpeed, jerkFactor, 0);
	}

	private static double distanceIntegral(double curAccel, double curSpeed,
			double jerkFactor, double delay) {
		return curSpeed * delay + curAccel * Math.pow(jerkFactor, delay)
				/ Math.pow(Math.log(jerkFactor), 2);
	}

	public double getDistanceToStartSlowdown(int curvesToPredict) {
		double decelerationEstimate = 
				analyzer.getInitialDecelerationEstimate(currentSpeed);
		double slowDownDistance = Double.MAX_VALUE;
		CurveInfo[] curves = getDistancesToNextCurves(curvesToPredict);
		for (int i = 0; i < curvesToPredict; i++) {
			double curveTargetSpeed = curves[i].piece.GetMaxEnterSpeed(curves[i].piece.optimalLane).speed; 
			/*System.out.println("---CURVESPEED: "+ curveTargetSpeed + " CHANGE: " + (curveTargetSpeed - currentSpeed)
					+ " DISTTO: " + curves[i].distanceTo);*/
			double t = getPredictedTime(curveTargetSpeed - currentSpeed, 
					decelerationEstimate, analyzer.jfStats[Analyzer.ZERO_THROTTLE].getMean());
			double s = getSlowDownDistance(decelerationEstimate, currentSpeed, 
					analyzer.jfStats[Analyzer.ZERO_THROTTLE].getMean(), t);
			if (t < 0) { // negative t means we are under the target speed of this curve
				s = -10000; // just set large negative value to avoid negative dist
			}
			double dist = curves[i].distanceTo - s;
			//System.out.println("---T: "+t+" S: "+s+" dist:"+dist);
			if (dist < slowDownDistance)
				slowDownDistance = dist;
		}
		return slowDownDistance;
	}
	
	public SpeedFriction getCurrentCurveMaxSpeed() {
		
		int i = piecePosition.pieceIndex;//+1;
		Piece curPiece = Main.racetrack.pieces.get(i);
 		return curPiece.maxSpeedToEnterWithoutCrash.get(curPiece.optimalLane);
	}
	
	
	public SpeedFriction getNextCurveMaxEntrySpeed() {
		int i = piecePosition.pieceIndex + 1;
		if (Main.racetrack.pieces.size() - 1 < i) {
			i = 0;
		}

		Piece nextPiece = Main.racetrack.pieces.get(i);
		while (nextPiece.isStraight()) {
			i++;
			if (Main.racetrack.pieces.size() - 1 < i) {
				i = 0;
			}
			nextPiece = Main.racetrack.pieces.get(i);
		}

		return nextPiece.GetMaxEnterSpeed(nextPiece.optimalLane);
 		//return nextPiece.maxSpeedToEnterWithoutCrash.get(nextPiece.optimalLane);
	}

	public double getNextCurveMinCrashSpeed() {
		int i = piecePosition.pieceIndex + 1;
		if (Main.racetrack.pieces.size() - 1 < i) {
			i = 0;
		}

		Piece nextPiece = Main.racetrack.pieces.get(i);
		while (nextPiece.isStraight()) {
			i++;
			if (Main.racetrack.pieces.size() - 1 < i) {
				i = 0;
			}
			nextPiece = Main.racetrack.pieces.get(i);
		}
		return nextPiece.minSpeedToEnterCausingCrash.get(nextPiece.optimalLane);
	}

	
	public Piece getNextCurve() {
		int i = piecePosition.pieceIndex + 1;
		if (Main.racetrack.pieces.size() - 1 < i) {
			i = 0;
		}

		Piece nextPiece = Main.racetrack.pieces.get(i);
		while (nextPiece.isStraight()) {
			i++;
			if (Main.racetrack.pieces.size() - 1 < i) {
				i = 0;
			}
			nextPiece = Main.racetrack.pieces.get(i);
		}
		return nextPiece;
	}

}
