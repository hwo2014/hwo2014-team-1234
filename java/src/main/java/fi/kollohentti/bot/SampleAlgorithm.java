package fi.kollohentti.bot;

import java.util.Hashtable;

import org.apache.commons.math3.fraction.*;

public class SampleAlgorithm implements Algorithm {

	private static final int CURVES_TO_PREDICT = 7;

	Piece currentPiece = null;
	Piece nextPiece = null;

	int nextPieceIndex = 0;
	private double RIGHT_ANGLE = 90;
	private double DRIFT_BOUNDARY = 10;
	private double MAX_DRIFT_ANGLE = RIGHT_ANGLE - DRIFT_BOUNDARY;
	private double targetSpeed = 0.0; // initial value is looked in first update

	private HenttiCar car;
	private CurveThrottleAdjuster cta;

	public SampleAlgorithm(HenttiCar henttiCar) {
		this.car = henttiCar;
		cta = new CurveThrottleAdjuster(car);
	}

	@Override
	public void update() {

		boolean switchingLane = false;
		currentPiece = Main.racetrack.pieces.get(car.piecePosition.pieceIndex);
		nextPieceIndex = car.piecePosition.pieceIndex + 1;
		if (car.piecePosition.lane.startLaneIndex != car.piecePosition.lane.endLaneIndex) {
			switchingLane = true;
		}
		if (nextPieceIndex > Main.racetrack.pieces.size() - 1) {
			nextPieceIndex = 0;
		}
		if (car.currentSpeed < 0.1) {
			car.throttle = 1;
			return;
		}
		nextPiece = Main.racetrack.pieces.get(nextPieceIndex);

		if (car.name == Main.ownCar.name && car.throttle == 0 && !car.isOut) {
			car.analyzer.addSample(0, car.currentSpeed,
					car.currentAcceleration, car.currentJerk,
					car.currentJerkFactor);
		}

		if (currentPiece.isStraight()) {

			// always set target speed to max on a straight
			targetSpeed = 0.1;
			if (Math.abs(car.angle) < 50) {
				targetSpeed = InitialSpeed.MAX_STRAIGHT_SPEED;
				handleSpeedInStraight(targetSpeed);
			} else
				car.throttle = 0.1;

		} // end of straight piece, we set initial throttle
			// to 1.0, so no need to reset it here, also there is on drifting in
			// straight

		// if we are inside a curve
		if (!currentPiece.isStraight()) {

			//SpeedFriction sf = car.getCurrentCurveMaxSpeed();
			//targetSpeed = 10;//sf.speed;
			SpeedFriction sf = car.getNextCurveMaxEntrySpeed();
			targetSpeed = sf.speed;//10;//sf.speed;

			handleSpeedInCurve(targetSpeed);
		}

		// finally we might be drifting, so handle it
		if (weAreDrifting()) {
			handleDrifting();
		}
	}

	public void handleDrifting() {
		car.throttle = 0.0;
	}

	// Function to set full throttle
	public void setThrottleToFull() {
		car.throttle = 1.0;
	}

	public boolean weAreDrifting() {

		// but in case we are sliding too much, we must EAS-UP the throttle
		double drift = Math.abs(car.angle) + Math.abs(car.hypotenuseAngle);

		if (MAX_DRIFT_ANGLE - Math.abs(drift) < 0) {
			return true;
		}

		return false;
	}

	public boolean needToAccelerate(double currentSpeed, double targetSpeed) {
		if (currentSpeed < targetSpeed) {
			return true;
		}
		return false;
	}

	@Override
	public AlgorithmType getNextAlgorithm() {
		// IF WE NEED EXTRA BOOST ETC... CHAIN ANOTHER ALGO HERE
		return null;
	}

	public void handleSpeedInCurve(double aTargetSpeed) {

		double v2 = car.getDistanceToStartSlowdown(CURVES_TO_PREDICT);
		/*
		 * if (!currentPiece.isStraight() && nextPiece.isStraight()) { v2*=2;
		 * System.out.println("SAFEDIST DOUBLED to "+v2);
		 * 
		 * }
		 */

		int safeDist = 12;
		if (car.turbo != null && car.turbo.usingTurbo) {
			safeDist *= (car.turbo.turboFactor * 2);
			// System.out.println("TURBO FACTOR ADDED TO SAFEDIST");
		}
		if (v2 < safeDist) { // TODO: 40 a safety limit..
			car.throttle = 0.0;

		} else {
			// we are not over the breaking barrier, just keep rollin.
			// setThrottleToFull();
			if (!car.changingLanes) {
				car.throttle = cta.getNextThrottle(aTargetSpeed,
						car.currentTick);
			}

		}
	}

	public void handleSpeedInStraight(double aTargetSpeed) {
		double v2 = car.getDistanceToStartSlowdown(CURVES_TO_PREDICT);
		int safeDist = 12;
		if (car.turbo != null && car.turbo.usingTurbo) {
			safeDist *= (car.turbo.turboFactor * 2);
			// System.out.println("TURBO FACTOR ADDED TO SAFEDIST");
		}
		if (v2 < safeDist) { // TODO: 40 a safety limit..
			car.throttle = 0.0;

		} else {
			setThrottleToFull();
		}
	}

	//

	// returns initial speed for a curve with given radius
	public double initialSpeedToRadiusSolver(double radius) {

		// System.out.println("LINE RADIUS: " +radius);
		// System.out.println("CURRENTSPEED: " +car.currentSpeed);
		// we add or reduce 0.2 to min target speed on every 10th radius...
		String key = new Double(radius).toString();
		double newSpeed = InitialSpeed.MIN_INITIAL_CURVE_TARGETSPEED;
		if (InitialSpeed.getInstance().initialValues.containsKey(key)) {
			newSpeed = InitialSpeed.getInstance().initialValues.get(key)
					.doubleValue();
			// System.out.println("KEY FOUND: " +newSpeed);
		}
		// double toAddSpeed = ((radius/10)*0.2);
		// System.out.println("NEW SPEED: " +newSpeed);
		return newSpeed;
	}

}
