package fi.kollohentti.bot;

public class TurboAvailable {
	public double turboDurationMilliseconds;
	public double turboDurationTicks;
	public double turboFactor; 
	
	public boolean usingTurbo;
	public boolean turboAvailable;
	public int startTick;
}
