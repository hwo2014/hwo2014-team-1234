package fi.kollohentti.bot;

import java.io.Serializable;

public class SpeedFriction implements Serializable{
	public Double speed;
	public Double previousSpeed;
	public Double friction;
}
