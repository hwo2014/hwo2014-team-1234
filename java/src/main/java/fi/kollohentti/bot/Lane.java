package fi.kollohentti.bot;

import java.io.Serializable;

class Lane implements Serializable
{
	public int distanceFromCenter;
	public int index;
	public int startLaneIndex;
	public int endLaneIndex;
	public double lenght;
}