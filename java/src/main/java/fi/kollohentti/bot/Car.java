package fi.kollohentti.bot;

class CarId
{
	public String name;
	public String color;
}

class CarDimensions
{
	public double length;
	public double width;
	public double guideFlagPosition;
}

class Car
{
	public CarId id;
	public double angle;
	public CarDimensions dimensions;
	public PiecePosition piecePosition;
}
