package fi.kollohentti.bot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class Main {

	final static boolean ONLINEMODE = true;
	final static boolean DEBUG = true;
	final static boolean CREATERACE = false;
	final static boolean MULTIRACE = false;
	final static String PASSWORD = "KOLLOTUS";
	final static boolean AUTOACCELERATE = false;
	
	JsonParser parser = new JsonParser();

	public static RaceSession raceSession = null;
	public static Track racetrack = null;

	private boolean firstThrottle = true; // set initial values if true
	private boolean laneSent = false;
	private Hashtable<String, HenttiCar> enemyCars = new Hashtable<String, HenttiCar>();
	private HenttiCar hentticar = null;
	private int gameTick;
	final Gson gson = new Gson();
	private PrintWriter writer;
	public static YourCar ownCar = new YourCar();

	public static void main(String... args) throws IOException {
		String host;
		int port;
		String botName;
		String botKey;
		String trackName;
		boolean officialStart = true;

		if (args.length < 1) {
			//host = "hakkinen.helloworldopen.com";
			// host = "senna.helloworldopen.com";
			host = "prost.helloworldopen.com";
			port = 8091;
			botName = "HergoJontti";
			botKey = "XtQrhvAK0gPCxg";
			// trackName = "elaeintarha";
			trackName = "usa";
			officialStart = false;
		} else {
			host = args[0];
			port = Integer.parseInt(args[1]);
			botName = args[2];
			botKey = args[3];
			trackName = null;
			officialStart = true;
		}
		System.out.println("Connecting to " + host + ":" + port + " as "
				+ botName + "/" + botKey);
		final PrintWriter writer;
		final BufferedReader reader;
		if (ONLINEMODE) {

			final Socket socket = new Socket(host, port);
			writer = new PrintWriter(new OutputStreamWriter(
					socket.getOutputStream(), "utf-8"));
			reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream(), "utf-8"));
		} else {
			writer = new PrintWriter(System.out);
			reader = new BufferedReader(new FileReader("test.txt"));
		}
		if (ONLINEMODE) {

			if (officialStart) {
				System.out.println("Preparing for Official Start");
				new Main(reader, writer, new Join(botName, botKey));
			} else {
				System.out.println("Preparing for Test Start");
				if (CREATERACE) {
					new Main(reader, writer, new CreateRace(botName, botKey,
							trackName, PASSWORD, 2));
				} else {
					if (MULTIRACE) {
						new Main(reader, writer, new JoinRace(botName, botKey,
								trackName, PASSWORD));
					} else {
						new Main(reader, writer, new JoinRace(botName, botKey,
								trackName));
					}

				}
			}

		} else {
			new Main(reader, writer, null);
		}
	}

	public Main(final BufferedReader reader, final PrintWriter writer,
			final SendMsg join) throws IOException {

		this.writer = writer;
		String line = null;

		if (ONLINEMODE) {

			System.out.println("Sending join...");
			send(join);
		}

		while ((line = reader.readLine()) != null) {
			// System.out.println(line);
			final MsgWrapper msgFromServer = gson.fromJson(line,
					MsgWrapper.class);

			if (msgFromServer.msgType.equals("carPositions")) {
				handleCarPositions(line);

			} else if (msgFromServer.msgType.equals("join")) {

				System.out.println("Joined");

			} else if (msgFromServer.msgType.equals("yourCar")) {

				System.out.println("Your car");
				JsonObject data = parser.parse(line).getAsJsonObject()
						.get("data").getAsJsonObject();

				ownCar = new Gson().fromJson(data, YourCar.class);

				// Will be called maybe in real game ?
			} else if (msgFromServer.msgType.equals("gameInit")) {
				handleGameInit(line);

			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");

				hentticar.throttle = 0.0;// end race
				send(new Throttle(hentticar.throttle, gameTick));

			} else if (msgFromServer.msgType.equals("gameStart")) {

				System.out.println("Game Start!");

				send(new Throttle(hentticar.throttle, gameTick));

			} else if (msgFromServer.msgType.equals("crash")) {
				handleCrash(line);
			}

			else if (msgFromServer.msgType.equals("spawn")) {
				System.out.println("SPAWN!!!!!");
				JsonObject data = parser.parse(line).getAsJsonObject()
						.get("data").getAsJsonObject();
				CarId carId = new Gson().fromJson(data, CarId.class);
				if (enemyCars.contains(carId.name)) {
					HenttiCar enemy = enemyCars.get(carId.name);
					enemy.isOut = false;
				} else if (carId.name.equals(ownCar.name)) {
					hentticar.isOut = false;
					hentticar.throttle = 1.0f;
				}
				// racetrack.pieces.get(hentticar.piecePosition.pieceIndex).crash(hentticar.id.name);
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				if (!hentticar.isOut) {
					JsonObject data = parser.parse(line).getAsJsonObject()
							.get("data").getAsJsonObject();
					TurboAvailable turbo = new Gson().fromJson(data,
							TurboAvailable.class);
					hentticar.turbo = turbo;

				}
			} else {

				System.out.println(line);
				if (ONLINEMODE) {
					// send(new Ping());
				}
			}
			/*
			 * try { Thread.sleep(10); } catch (InterruptedException e) { //
			 * TODO Auto-generated catch block e.printStackTrace(); }
			 */
		}
	}
	
	private void handleCrash(String line) {

		JsonObject data = parser.parse(line).getAsJsonObject().get("data")
				.getAsJsonObject();

		CarId carId = new Gson().fromJson(data, CarId.class);

		if (enemyCars.containsKey(carId.name)) {
			HenttiCar enemy = enemyCars.get(carId.name);
			/*double inPiecePercentage = enemy.piecePosition.inPieceDistance
					/ enemy.piece
							.getLength(enemy.piecePosition.lane.endLaneIndex);*/

			enemy.isOut = true;
			racetrack.pieces.get(enemy.piecePosition.pieceIndex).crash(
					carId.name, enemy.piecePosition.lane.endLaneIndex,
					0);
		} else if (carId.name.equals(ownCar.name)) {
			double inPiecePercentage = hentticar.piecePosition.inPieceDistance
					/ hentticar.piece
							.getLength(hentticar.piecePosition.lane.endLaneIndex);
			racetrack.pieces.get(hentticar.piecePosition.pieceIndex).crash(
					hentticar.id.name,
					hentticar.piecePosition.lane.endLaneIndex,
					inPiecePercentage);

			int beforePieceIndex = hentticar.piecePosition.pieceIndex;
			/*double[] multipliers = new double[] { 0.95, 0.97, 0.99 };
			for (int i = 0; i < 3; i++) {
				do {
					beforePieceIndex--;// =
										// hentticar.piecePosition.pieceIndex--;
					if (beforePieceIndex < 0)
						beforePieceIndex = racetrack.pieces.size() - 1;
					System.out.println(beforePieceIndex
							+ "  "
							+ racetrack.pieces.get(beforePieceIndex)
									.isStraight());
				} while (racetrack.pieces.get(beforePieceIndex).isStraight());
				racetrack.pieces.get(beforePieceIndex).slowdown(multipliers[i]);// *inPiecePercentage);
			}*/
			
			
			 beforePieceIndex--; if (beforePieceIndex < 0) beforePieceIndex =
			 racetrack.pieces.size() - 1;
			 racetrack.pieces.get(beforePieceIndex).slowdown(0.95);
			 
			 beforePieceIndex--; if (beforePieceIndex < 0) beforePieceIndex =
			 racetrack.pieces.size() - 1;
			 racetrack.pieces.get(beforePieceIndex).slowdown(0.98);
			

			// crash removes turbo availability
			hentticar.turbo = null;
			hentticar.useTurbo = false;
			hentticar.isOut = true;
		}
	}
	
	private void send(final SendMsg msg) {
		// System.out.println(msg.toJson());
		writer.println(msg.toJson());
		writer.flush();
	}

	private void handleCarPositions(String line) {
		// we get one car position message from server before gamestart, where
		// we
		// actually send the throttle
		JsonArray dataArray = parser.parse(line).getAsJsonObject()
				.getAsJsonArray("data");
		JsonPrimitive gameTickObject = parser.parse(line).getAsJsonObject()
				.getAsJsonPrimitive("gameTick");
		if (gameTickObject != null) {
			gameTick = new Gson().fromJson(gameTickObject, Integer.class);
		} else {
			gameTick = 0;
		}
		// System.out.println(gameTick);
		for (JsonElement je : dataArray) {
			Car car = new Gson().fromJson(je, Car.class);
			if (car.id.name.equals(ownCar.name)) {
				hentticar.setAngle(car.angle);
				hentticar.piecePosition = car.piecePosition;
				hentticar.currentLane = car.piecePosition.lane;
				hentticar.currentLane.index = hentticar.currentLane.endLaneIndex;
				hentticar.id = car.id;
				hentticar.currentTick = gameTick;
				Piece currentPiece = racetrack.pieces
						.get(car.piecePosition.pieceIndex);
				hentticar.piece = currentPiece;

				// double multiplier =
				// currentPiece.getLength(hentticar.currentLane.index) /
				// currentPiece.getCenterLineLength();
				// --hentticar.piecePosition.inPieceDistance *= multiplier;

				// just overwrite index, it does not come as "index" in the
				// json.

				if (firstThrottle) {

					// set initialize throttle for car (100%), but dont send yet
					hentticar.throttle = 1.0;

					firstThrottle = false; // mark done

				} else {

					if (!hentticar.isOut)
						hentticar.Update();

					System.out.println(hentticar.toCSV());

					if (Main.ONLINEMODE) {

						int i = hentticar.piecePosition.pieceIndex + 1;
						if (i > racetrack.pieces.size() - 1) {
							i = 0;
						}
						Piece nextPiece = racetrack.pieces.get(i);
						// send (new Throttle(hentticar.throttle));

						if (nextPiece._switch
								&& hentticar.currentLane.index != nextPiece.optimalLane
								&& !laneSent) {
							// System.out.println(hentticar.currentLane.index);
							String lane = "Right";
							if (nextPiece.optimalLane < hentticar.currentLane.index) {
								lane = "Left";
							} else {
								lane = "Right";
							}
							hentticar.changingLanes = true;
							send(new ChangeLane(lane, gameTick));
							// System.out.println("Lane change sent: " + lane);
							laneSent = true;
						} else {
							if (hentticar.useTurbo) {
								System.out
										.println("!!!!!TURBO SENT!!! KORBO TUNTTI!!!!");
								send(new TurboMessage(gameTick));
								hentticar.useTurbo = false;

							} else {
								send(new Throttle(hentticar.throttle, gameTick));
							}
							if (!nextPiece._switch) {
								laneSent = false;
								hentticar.changingLanes = false;
							}
						}

					}

				}

			} else {

				if (!enemyCars.containsKey(car.id.name)) {
					HenttiCar enemy = new HenttiCar();
					enemy.setAngle(car.angle);
					enemy.piecePosition = car.piecePosition;
					enemy.currentLane = car.piecePosition.lane;
					enemy.currentLane.index = enemy.currentLane.endLaneIndex;
					enemy.id = car.id;
					enemyCars.put(car.id.name, enemy);
					enemy.Update();
				} else {
					HenttiCar enemy = enemyCars.get(car.id.name);
					enemy.setAngle(car.angle);
					enemy.piecePosition = car.piecePosition;
					enemy.currentLane = car.piecePosition.lane;
					enemy.currentLane.index = enemy.currentLane.endLaneIndex;
					enemy.Update();
				}
			}
		}

	}

	private void handleGameInit(String line) {
		System.out.println("Race init");
		racetrack = new Track();

		JsonObject race = parser.parse(line).getAsJsonObject().get("data")
				.getAsJsonObject().get("race").getAsJsonObject();

		JsonObject track = race.get("track").getAsJsonObject();
		JsonElement session = race.get("raceSession").getAsJsonObject();

		raceSession = new Gson().fromJson(session, RaceSession.class);
		if (DEBUG) {
			System.out.println("-----------------");
			System.out.println("Race has laps: " + raceSession.laps);
			System.out.println("Max Lap time: " + raceSession.maxLapTimeMs);
			System.out.println("Quick race: " + raceSession.quickRace);
			System.out.println("-----------------");
		}

		String id = track.get("id").toString();
		String name = track.get("name").toString();
		racetrack.id = id;
		racetrack.name = name;

		JsonArray laneArray = track.getAsJsonArray("lanes");
		for (JsonElement lns : laneArray) {

			Lane lane = new Gson().fromJson(lns, Lane.class);
			if (DEBUG) {
				System.out.println("-----------------");
				System.out.println("Lane Index: " + lane.index);
				System.out.println("Lane Offset: " + lane.distanceFromCenter);
				System.out.println("-----------------");
			}

			racetrack.lanes.add(lane);
		}

		JsonArray dataArray = track.getAsJsonArray("pieces");
		int pieceIndex = 0;
		for (JsonElement je : dataArray) {

			Piece piece = new Gson().fromJson(je, Piece.class);
			System.out.println("PIECE " + pieceIndex + " RADIUS:"
					+ piece.radius + " ANGLE:" + piece.angle);
			// if (piece.angle!=0) {

			// piece.length = Math.abs(((piece.radius*2) * Math.PI *
			// (piece.angle/360)));
			piece.init(racetrack.lanes, pieceIndex);
			pieceIndex++;
			
			racetrack.rotation += piece.angle;
			// }

			// racetrack.length += piece.length;
			/*
			 * if(DEBUG) {
			 * 
			 * System.out.println("-----------------");
			 * System.out.println("Piece Lenght: " +piece.length);
			 * System.out.println("-----------------");
			 * 
			 * }
			 */
			racetrack.pieces.add(piece);

		}
		racetrack.calculateOptimalLanes();
		if (DEBUG) {
			System.out.println("-----------------");
			System.out.println("Track has pieces: " + racetrack.pieces.size());
			System.out.println("Track rotation is: " + racetrack.rotation);
			System.out.println("Track lenght is: " + racetrack.length);
			System.out.println("-----------------");
		}

		JsonArray carsArray = race.getAsJsonArray("cars");
		raceSession.carList.clear(); // clear current car list

		for (JsonElement lcars : carsArray) {
			Car car = new Gson().fromJson(lcars, Car.class);

			// TODO: check if hentticar, create
			if (car.id.name.equals(ownCar.name)) {

				hentticar = new HenttiCar();
				hentticar.name = ownCar.name;
				hentticar.color = ownCar.color;
				hentticar.dimensions = new CarDimensions();
				// set hentticar dimensions

				hentticar.dimensions.length = car.dimensions.length;
				hentticar.dimensions.width = car.dimensions.width;
				hentticar.dimensions.guideFlagPosition = car.dimensions.guideFlagPosition;
				double y = hentticar.dimensions.length
						- hentticar.dimensions.guideFlagPosition;
				double x = hentticar.dimensions.width / 2;
				hentticar.hypotenuse = Math.sqrt(((y * y) + (x * x)));
				hentticar.hypotenuseAngle = Math.abs(Math.atan(x / y)
						* (180 / Math.PI));
				hentticar.maxAngle = 90 - hentticar.hypotenuseAngle;

				// System.out.println("--------");
				// System.out.println("Hypotenuse: " + hentticar.hypotenuse);
				// System.out.println("Hypotenuse Angle: "
				// + hentticar.hypotenuseAngle);
				// System.out.println("--------");

			}

			// this car list is used to monitor competitors
			raceSession.carList.add(car);

		}

		if (DEBUG) {

			System.out.println("-----------------");
			System.out.println("Cars in a race:" + raceSession.carList.size());
			System.out.println("-----------------");

			for (int i = 0; i < raceSession.carList.size(); i++) {

				Car c = raceSession.carList.get(i);

				System.out.println("-----------------");
				System.out.println("Car Name:" + c.id.name);
				System.out.println("Car Color:" + c.id.color);
				System.out.println("Length:" + c.dimensions.length);
				System.out.println("Width:" + c.dimensions.width);
				System.out.println("GuideFLag:"
						+ c.dimensions.guideFlagPosition);
				System.out.println("-----------------");

			}
		}

	}

}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();

	protected Integer gameTick;
}

class MsgWrapper {
	public final String msgType;
	public final Object data;
	public final Integer gameTick;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
		this.gameTick = null;
	}

	MsgWrapper(final String msgType, final Object data, final Integer gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick);
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Bot {
	public final String name;
	public final String key;

	public Bot(String n, String k) {
		name = n;
		key = k;
	}
}

class JoinRace extends SendMsg {
	public final String trackName;
	public final String password;
	public Bot botId;

	public final int carCount;

	JoinRace(final String name, final String key, final String trackName) {
		this.botId = new Bot(name, key);
		this.trackName = trackName;
		password = null;
		carCount = 1;
	}

	JoinRace(final String name, final String key, final String trackName,
			final String password) {
		this.botId = new Bot(name, key);
		this.trackName = trackName;
		this.password = password;
		carCount = 3;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}

class CreateRace extends SendMsg {
	public final String trackName;
	public final String password;
	public Bot botId;

	public int carCount = 1;

	CreateRace(final String name, final String key, final String trackName,
			final String password, final int carCount) {
		this.botId = new Bot(name, key);
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	@Override
	protected String msgType() {
		return "createRace";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double data;

	public Throttle(double value, int currentTick) {
		this.data = value;
		if (data < 0)
			data = 0;
		if (data > 1)
			data = 1;
		this.gameTick = currentTick;
	}

	@Override
	protected Object msgData() {
		return data;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}

class ChangeLane extends SendMsg {
	private String Lane;

	public ChangeLane(String value, int currentTick) {
		this.Lane = value;
		this.gameTick = currentTick;
	}

	@Override
	protected Object msgData() {
		return Lane;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}

class TurboMessage extends SendMsg {
	public final String message = "KORBO TUNTTI!";

	public TurboMessage(int gameTick) {
		this.gameTick = gameTick;
	}

	@Override
	protected Object msgData() {
		return message;
	}

	@Override
	protected String msgType() {
		return "turbo";
	}
}