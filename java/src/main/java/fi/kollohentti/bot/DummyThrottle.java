package fi.kollohentti.bot;

public class DummyThrottle implements Algorithm {

	private HenttiCar car;

	private int jerkAnalyzerFactor;
	private int jerkAnalyzerFactorIndex = 0;
	private boolean jerkDataCollected = false;

	public DummyThrottle(HenttiCar henttiCar) {
		this.car = henttiCar;
		jerkAnalyzerFactor = Analyzer.THROTTLEFACTORS[0];
	}

	@Override
	public void update() {
		if (!jerkDataCollected) {
			
			car.throttle = Analyzer.FACTOR_TO_THROTTLE_MAP
					.get(jerkAnalyzerFactor);
			
			car.analyzer.addSample(
					jerkAnalyzerFactor, 
					car.currentSpeed, 
					car.currentAcceleration, 
					car.currentJerk, 
					car.currentJerkFactor);
			//System.out.print(car.analyzer.toString());
			if (car.analyzer.speedStats[jerkAnalyzerFactor].getN() >= 5) {
				if (++jerkAnalyzerFactorIndex == Analyzer.THROTTLEFACTORS.length) {
					jerkDataCollected = true;
				} else {
					jerkAnalyzerFactor = Analyzer.THROTTLEFACTORS[jerkAnalyzerFactorIndex];
				}
			}
		}

	}

	@Override
	public AlgorithmType getNextAlgorithm() {
		// TODO Auto-generated method stub
		if (jerkDataCollected) {
			return AlgorithmType.BASIC_THROTTLE;
		} else return null;
	}

	// 0.8 JF: 1.0213564802784196
	// 0.7 JF: 1.0202718000997641
	// 0.6 JF: 1.0187187459093638

}
