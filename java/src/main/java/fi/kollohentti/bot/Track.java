package fi.kollohentti.bot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Track implements Serializable{

	public String id;
	public String name;
	public double length;
	public double rotation; // clockwise is positive, counter clockwise negative, 0 if circular
	public boolean isTrackAccellerationCalculated = false;
	
	public List<Piece> pieces = new ArrayList<Piece>();
	public List<Lane> lanes = new ArrayList<Lane>();
	

	private List<Piece> getSwitchPieces() {
		ArrayList<Piece> ret = new ArrayList<Piece>();
		for (Piece piece : pieces) {
			if (piece._switch) {
				ret.add(piece);
			}
		}
		return ret;
	}
	
	private double getDistanceUntilNextSwitch(Piece fromPiece, int lane) {
		double distance = fromPiece.getLength(lane);
		int fromPieceIndex = pieces.indexOf(fromPiece);
		++fromPieceIndex;
		if (fromPieceIndex>pieces.size()-1) {
			fromPieceIndex = 0;
		}
		while (!pieces.get(fromPieceIndex)._switch) {
			distance+=pieces.get(fromPieceIndex).getLength(lane);
			++fromPieceIndex;
			if (fromPieceIndex>pieces.size()-1) {
				fromPieceIndex = 0;
			}	
		}
		distance+=pieces.get(fromPieceIndex).getLength(lane)/2;
		return distance;
	}
	
	public void calculateOptimalLanes() {
		List<Piece> switchPieces = getSwitchPieces();
		double minimalLaneLength = Double.MAX_VALUE;
		int optimalLane = 0;
		int laneCount = lanes.size();
		
		for (Piece p:switchPieces) {
			minimalLaneLength = Double.MAX_VALUE;
			for (int i = 0;i<laneCount;i++) {
				double laneLength = getDistanceUntilNextSwitch(p,i);
				if (laneLength<minimalLaneLength) {
					optimalLane = i;
					minimalLaneLength = laneLength; 
				}
			}
			p.optimalLane = optimalLane;
		}
		//switchPieces.get(switchPieces.size()-1).optimalLane = 1;
		
		int first = -1;
		int oLane = -1;
		// stores optimal lane to all pieces, not only switch pieces:
		for (int i=0;i<pieces.size()-1;i++) {
			Piece piece = pieces.get(i);
			if (piece._switch) {
				if (first==-1) first = i; // first switch piece index stored so all before that can be filled after this loop.
				oLane = piece.optimalLane;
			}
			else if (first!=-1)
			{
				piece.optimalLane = oLane; 
			}
		}
		for (int i=0;i<first;i++) {
			Piece piece = pieces.get(i);
			piece.optimalLane = oLane;
		}
		int test = 1;
	}
		
	public Piece getFirstCurve() {
		Piece ret = null;
		for (Piece p:this.pieces) {
			if ( !p.isStraight() ) {
				p=ret;
			}
		}
		return ret;
	}

	
	
	public double getMaxAngleSpeed(double radius, double coefficientFriction) {

		return Math.sqrt(Math.abs(radius) * coefficientFriction);

	}
	
}
