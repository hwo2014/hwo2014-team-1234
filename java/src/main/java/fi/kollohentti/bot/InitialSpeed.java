package fi.kollohentti.bot;

import java.util.Hashtable;

public class InitialSpeed {

	public static double MIN_INITIAL_CURVE_TARGETSPEED = 5.0;
    public static double MAX_STRAIGHT_SPEED = 15.0;
    public static double INITIALSPEED_MULTIPLIER = 1.02;
    
    
    public Hashtable<String, Double> initialValues = new Hashtable<String,Double>();
    
    private static InitialSpeed instance = null;
    
    protected InitialSpeed() {
    	
    	initialValues.put("10.0", new Double(1.0));
		initialValues.put("20.0", new Double(2.5));
		initialValues.put("30.0", new Double(3.0));
		initialValues.put("40.0", new Double(3.5));
		initialValues.put("50.0", new Double(4.5));
		initialValues.put("60.0", new Double(5.5));
		initialValues.put("65.0", new Double(5.5));
		initialValues.put("67.0", new Double(5.5));
		initialValues.put("70.0", new Double(6.0));
		initialValues.put("75.0", new Double(6.0));
		initialValues.put("80.0", new Double(6.0));
		initialValues.put("90.0", new Double(6.0));
		initialValues.put("100.0", new Double(6.0));
		initialValues.put("110.0", new Double(7.0));
		initialValues.put("120.0", new Double(7.5));
		initialValues.put("130.0", new Double(7.5));
		initialValues.put("140.0", new Double(8.0));
		initialValues.put("150.0", new Double(8.0));
		initialValues.put("160.0", new Double(8.5));
		initialValues.put("170.0", new Double(8.5));
		initialValues.put("180.0", new Double(10.0));
		initialValues.put("190.0", new Double(10.0));
		initialValues.put("200.0", new Double(10.1));
		initialValues.put("210.0", new Double(10.1));
		initialValues.put("220.0", new Double(10.2));
		initialValues.put("230.0", new Double(10.2));
		initialValues.put("240.0", new Double(10.2));
		initialValues.put("250.0", new Double(11.2));
		initialValues.put("260.0", new Double(11.2));
		initialValues.put("270.0", new Double(11.2));
		initialValues.put("280.0", new Double(12.2));
		initialValues.put("290.0", new Double(12.2));
		initialValues.put("300.0", new Double(12.2));
		initialValues.put("310.0", new Double(12.2));
    }
    
    public static InitialSpeed getInstance() {
        if(instance == null) {
           instance = new InitialSpeed();
        }
        return instance;
     }
	
}
