package fi.kollohentti.bot;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

class Piece implements Serializable {
	private double length;

	@SerializedName("switch")
	public boolean _switch;

	public List<Lane> lanes;
	public double radius;
	public double angle;
	public double[] laneLengths;
	public int optimalLane;
	public int index;

	public HashMap<Double, Double> SpeedToAngleChangeMap = new HashMap<Double, Double>();

	public Hashtable<Integer, SpeedFriction> maxSpeedToEnterWithoutCrash = new Hashtable<Integer, SpeedFriction>();
	public Hashtable<Integer, Double> minSpeedToEnterCausingCrash = new Hashtable<Integer, Double>();
	public Hashtable<Integer, Double> maxAngleWhileEnteringWithoutCrash = new Hashtable<Integer, Double>();

	public Hashtable<String, Hashtable<Integer, Double>> latestEnterSpeeds = new Hashtable<String, Hashtable<Integer, Double>>();
	public Hashtable<String, Hashtable<Integer, Double>> latestEnterAngles = new Hashtable<String, Hashtable<Integer, Double>>();

	private static final long serialVersionUID = -326130804688947839l;
	
	public boolean crashedStraight = false;
	
	public boolean isStraight() {
		return angle == 0 && !crashedStraight;
	}

	public boolean isRightTurn() {
		return angle > 0;
	}

	public boolean isLeftTurn() {
		return angle < 0;
	}

	public int getLaneDistaceFromCenter(int laneIndex) {

		Lane l = lanes.get(laneIndex);
		return l.distanceFromCenter;
	}

	public void init(List<Lane> lanes, int index) {
		this.lanes = lanes;
		this.index = index;
		laneLengths = new double[lanes.size()];
		int i = 0;
		// length = Math.abs((((radius)*2) * Math.PI * (angle/360)));
		for (Lane lane : lanes) {
			// A positive value tells that the lanes is to the right from the
			// center line while a
			// negative value indicates a lane to the left from the center
			if (this.isStraight()) {
				SpeedFriction sf = new SpeedFriction();
				sf.speed = 0d;
				sf.friction = 0d;
				maxSpeedToEnterWithoutCrash.put(i, sf);
				minSpeedToEnterCausingCrash.put(i, (double) 0);
				maxAngleWhileEnteringWithoutCrash.put(i, 0d);
				laneLengths[i++] = length;

			} else if (this.isRightTurn()) {
				SpeedFriction sf = new SpeedFriction();
				sf.speed = 0d;
				sf.friction = 0d;
				maxSpeedToEnterWithoutCrash.put(i, sf);
				// put 0 to initial value, so we can compare if someone is
				// actually chashed...
				minSpeedToEnterCausingCrash.put(i, (double) 0);
				maxAngleWhileEnteringWithoutCrash.put(i, 0d);
				laneLengths[i++] = Math
						.abs((((radius - lane.distanceFromCenter) * 2)
								* Math.PI * (angle / 360)));

			}

			if (this.isLeftTurn()) {

				SpeedFriction sf = new SpeedFriction();
				sf.speed = 0d;
				sf.friction = 0d;
				maxSpeedToEnterWithoutCrash.put(i, sf);
				// put 0 to initial value, so we can compare if someone is
				// actually chashed...
				minSpeedToEnterCausingCrash.put(i, (double) 0);
				laneLengths[i++] = Math
						.abs((((radius + lane.distanceFromCenter) * 2)
								* Math.PI * (angle / 360)));

			}

		}
	}

	public double getLength(int laneIndex) {
		return isStraight() ? length : laneLengths[laneIndex];
	}

	public double getCenterLineLength() {
		return radius != 0 ? angle : length;
	}

	// returns initial speed for a curve with given radius
	public double initialSpeedToRadiusSolver(double radius) {

		String key = new Double(radius).toString();
		double newSpeed = InitialSpeed.MIN_INITIAL_CURVE_TARGETSPEED;
		if (radius == 0.0) {
			newSpeed = InitialSpeed.MAX_STRAIGHT_SPEED;
		}

		if (InitialSpeed.getInstance().initialValues.containsKey(key)) {
			newSpeed = InitialSpeed.getInstance().initialValues.get(key)
					.doubleValue();
		}
		return newSpeed;
	}

	public SpeedFriction GetMaxEnterSpeed(int lane) {

		if (isStraight()) {
			SpeedFriction sf = new SpeedFriction();
			sf.speed = (double) 10;
			return sf;
		}
		double crashSpeed = minSpeedToEnterCausingCrash.get(lane);
		SpeedFriction sf = maxSpeedToEnterWithoutCrash.get(lane);
		if (sf.speed == 0) {
			double initialTarget = initialSpeedToRadiusSolver(radius);
			SpeedFriction isf = new SpeedFriction();
			if (crashSpeed > 0 && crashSpeed < initialTarget) {
				//System.out.println("MOLLUKKA");
				initialTarget = crashSpeed * 0.9;
			}
			isf.speed = initialTarget;
			return isf;
		}
		return sf;
	}

	public double GetMinCrashSpeed(int lane) {
		return minSpeedToEnterCausingCrash.get(lane);
	}

	public void crash(String name, int lane, double piecePositionPercentage) {
		System.out.println("CAR CRASHED in piece: " + index + " : " + name + " %: "+piecePositionPercentage);
		if (!latestEnterSpeeds.containsKey(name)) {
			System.out.println("EIOO!!");
			return;
		}
		if (!crashedStraight) crashedStraight = true;
		
		double speed = latestEnterSpeeds.get(name).get(lane);

		System.out.println("CRASH SPEED:" + speed);
		
		if (speed>maxSpeedToEnterWithoutCrash.get(lane).speed) System.out.println("OVER THE MAX LIMIT!:"+maxSpeedToEnterWithoutCrash.get(lane).speed+", "+speed);
		
		if (!isStraight() && speed < 10) {
			// first crash on this piece
			if (minSpeedToEnterCausingCrash.get(lane) == 0.0) {
				System.out.println("REPLACING WITH 1: " + speed * 0.95);
				minSpeedToEnterCausingCrash.replace(lane, speed * 0.95);
			} else if (latestEnterSpeeds.get(name).get(lane) * 0.95 < minSpeedToEnterCausingCrash
					.get(lane)) {
				System.out.println("REPLACING WITH 2: " + speed * 0.95);
				minSpeedToEnterCausingCrash.replace(lane, speed * 0.95);
			} else {
				double latestval = minSpeedToEnterCausingCrash.get(lane);
				System.out.println("REPLACING WITH 3: " + latestval * 0.98);
				minSpeedToEnterCausingCrash.replace(lane, latestval * 0.98);
			}
			if (maxSpeedToEnterWithoutCrash.get(lane).speed > speed) {
				System.out.println("REPLACING MAX ENTER "
						+ maxSpeedToEnterWithoutCrash.get(lane).speed
						+ " WITH: " + speed * 0.97);
				SpeedFriction sf = new SpeedFriction();
				sf.speed = speed * 0.97;

				maxSpeedToEnterWithoutCrash.replace(lane, sf);
			}
		}
		latestEnterSpeeds.remove(name);
		latestEnterAngles.remove(name);
	}

	public void carExited(String name, int lane, double speed) {

		if (!latestEnterSpeeds.containsKey(name)) {
			if (latestEnterAngles.containsKey(name))
				latestEnterAngles.remove(name);
			//System.out.println("HEILURI!");
			return;
		}
		//speed = latestEnterSpeeds.get(name).get(lane);
		System.out.println(index+" ENTERED at:"+speed+" / "+maxSpeedToEnterWithoutCrash.get(lane).speed+" / "+minSpeedToEnterCausingCrash.get(lane));
		int radiusOffset = getLaneDistaceFromCenter(lane);
		double rad = getLaneRadius(radiusOffset);
		if (Main.AUTOACCELERATE) {
			if (speed < 10 && !isStraight()) {

				if (speed < maxSpeedToEnterWithoutCrash.get(lane).speed)
					speed = maxSpeedToEnterWithoutCrash.get(lane).speed;

				double minspeedtocrash = minSpeedToEnterCausingCrash.get(lane);
				if (minspeedtocrash == 0)
					minspeedtocrash = 10;
				if (speed * 1.02 < minspeedtocrash) {
					//System.out.println("KERTAA 1.02!");
					SpeedFriction sf = new SpeedFriction();
					sf.speed = speed * 1.02;
					maxSpeedToEnterWithoutCrash.replace(lane, sf);
				} else if (speed < minspeedtocrash) {

					//System.out.println("PUOLITETAAN!");
					speed = (speed + minspeedtocrash) / 2;
					SpeedFriction sf = new SpeedFriction();
					sf.speed = speed;
					maxSpeedToEnterWithoutCrash.replace(lane, sf);
				}
				else {
					//System.out.println("SETATAAN SAMA!");
					minSpeedToEnterCausingCrash.replace(lane, minspeedtocrash*1.01);
					//maxSpeedToEnterWithoutCrash.replace(lane, sf);
				}
			}
		} else {
			// NO AUTOACCELERATION, JUST REPLACE:
			if (speed < 10 && !isStraight()) {

				if (speed < maxSpeedToEnterWithoutCrash.get(lane).speed)
					speed = maxSpeedToEnterWithoutCrash.get(lane).speed;

				SpeedFriction sf = new SpeedFriction();
				sf.speed = speed;
				maxSpeedToEnterWithoutCrash.replace(lane, sf);
			}
		}
		latestEnterSpeeds.remove(name);
		latestEnterAngles.remove(name);
	}

	/*
	 * public void carExited(String name, int lane, double radius) {
	 * //System.out.println("CAR EXITED: "+this); if
	 * (!latestEnterSpeeds.containsKey(name)) return;
	 * 
	 * if
	 * (latestEnterSpeeds.get(name).get(lane)>maxSpeedToEnterWithoutCrash.get(
	 * lane).speed) { SpeedFriction sf = new SpeedFriction(); sf.speed =
	 * latestEnterSpeeds.get(name).get(lane); int radiusOffset =
	 * getLaneDistaceFromCenter(lane); double rad = getLaneRadius(radiusOffset);
	 * //if (rad!=0) { double friction =
	 * Main.racetrack.updateCoefficientFriction(rad,
	 * latestEnterSpeeds.get(name).get(lane)); //} sf.friction = friction;
	 * maxSpeedToEnterWithoutCrash.replace(lane, sf);
	 * //maxAngleWhileEnteringWithoutCrash
	 * .replace(lane,latestEnterAngles.get(name).get(lane));
	 * maxAngleWhileEnteringWithoutCrash
	 * .replace(lane,latestEnterAngles.get(name).get(lane)); }
	 * latestEnterSpeeds.remove(name); latestEnterAngles.remove(name); }
	 */

	public double getLaneRadius(double radiusOffset) {
		double newRadius = 0.0;
		if (isRightTurn()) {
			newRadius = this.radius - radiusOffset;
		} else if (isLeftTurn()) {
			newRadius = this.radius + radiusOffset;
		}

		return newRadius;
	}

	public void slowdown(double factor) {
		SpeedFriction sf = maxSpeedToEnterWithoutCrash.get(optimalLane);
		SpeedFriction newsf = new SpeedFriction();
		if (sf != null) {
			System.out.println("SLOWDOWN A BIT! "+sf.speed +"    "+sf.speed*factor);
			newsf.speed = sf.speed * factor;
			maxSpeedToEnterWithoutCrash.replace(optimalLane, newsf);
		}
	}
}
