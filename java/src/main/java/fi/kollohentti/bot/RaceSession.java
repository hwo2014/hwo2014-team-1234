package fi.kollohentti.bot;

import java.util.ArrayList;
import java.util.List;

public class RaceSession {

	public int laps;
	public int maxLapTimeMs;
	public boolean quickRace;

	//All cars, including our own, this car list is used to monitor competitors
	public List<Car> carList = new ArrayList<Car>();
	
}
