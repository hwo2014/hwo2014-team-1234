package fi.kollohentti.bot;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class CurveThrottleAdjuster {

	static final int TICK_INTERVAL = 1; // set e.g. to 2 if you want to throttle
										// every other tick

	HenttiCar car;
	HashMap<Double, DescriptiveStatistics> throttleStats; // map targetspeed ->
															// stats

	public CurveThrottleAdjuster(HenttiCar car) {
		this.car = car;
		throttleStats = new HashMap<Double, DescriptiveStatistics>();
	}

	public double getNextThrottle(double targetSpeed, int tick) {
		double throttle = 0.0;
		//System.out.println("CTA TARGETSPEED:"+targetSpeed);
		if (targetSpeed<0) {
			targetSpeed = 0;
		}
		
		if (tick % TICK_INTERVAL == 0) {
			DecimalFormat df = new DecimalFormat("##.###");
			df.setRoundingMode(RoundingMode.FLOOR);
			String test = df.format(targetSpeed);
			Double d = Double.valueOf(test.replace(',', '.'));//Double.valueOf(targetSpeed);
			DescriptiveStatistics stats = null;

			if (!throttleStats.containsKey(d)) {
				// first time with this target speed
				System.out.println("CTA FIRST TIME");
				double initialThrottleGuess = targetSpeed / 9.9; // this is
																	// probably
																	// horrible
																	// :)
				stats = new DescriptiveStatistics();
				throttleStats.put(d, stats);
				throttle = initialThrottleGuess;

			} else {
				// not first time with this target speed
				double speedTargetFactor = targetSpeed / car.currentSpeed;
				stats = throttleStats.get(d);
				double[] throttles = stats.getValues();
				double lastThrottle = throttles[throttles.length - 1];
				//lastThrottle = stats.getMean();
				
				//System.out.println("tgt :" + targetSpeed + " stf :" + speedTargetFactor + " lt :" + lastThrottle);

				if (speedTargetFactor < 1) {
					// going too fast, need to slow down (probably fucked up
					// already..)
					throttle = lastThrottle * speedTargetFactor;
				} else {
					// below target speed, should throttle more
					throttle = lastThrottle * speedTargetFactor;
				}
			}
			if (throttle > 1.0)
				throttle = 1.0;
			if (throttle < 0.0)
				throttle = 0.0;

			stats.addValue(throttle);
		}
		//System.out.println("returning throttle " + throttle);
		return throttle;

	}

}
