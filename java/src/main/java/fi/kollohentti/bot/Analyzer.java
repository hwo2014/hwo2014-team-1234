package fi.kollohentti.bot;

import java.util.HashMap;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.regression.SimpleRegression;

public class Analyzer {

	public static final int THROTTLE_LEVEL_COUNT = 6;
	
	public static final int ZERO_THROTTLE = 0;
	public static final int THROTTLE_0_2 = 1;
	public static final int THROTTLE_0_4 = 2;
	public static final int THROTTLE_0_6 = 3;
	public static final int THROTTLE_0_8 = 4;
	public static final int THROTTLE_1_0 = 5;

	public static final int[] THROTTLEFACTORS 
		= {THROTTLE_0_2, THROTTLE_0_4, THROTTLE_0_6, THROTTLE_0_8, THROTTLE_1_0, ZERO_THROTTLE};
	
	public static final HashMap<Integer, Double> FACTOR_TO_THROTTLE_MAP = new HashMap<Integer, Double>();
	
	private static final double MIN_JERKFACTOR = 0.8;
	private static final double MAX_JERKFACTOR = 1.2;

	public DescriptiveStatistics[] speedStats = new DescriptiveStatistics[THROTTLE_LEVEL_COUNT];
	public DescriptiveStatistics[] accelStats = new DescriptiveStatistics[THROTTLE_LEVEL_COUNT];
	public DescriptiveStatistics[] jerkStats = new DescriptiveStatistics[THROTTLE_LEVEL_COUNT];
	public DescriptiveStatistics[] jfStats = new DescriptiveStatistics[THROTTLE_LEVEL_COUNT];
	
	static {
		FACTOR_TO_THROTTLE_MAP.put(ZERO_THROTTLE,  0d);
		FACTOR_TO_THROTTLE_MAP.put(THROTTLE_0_2, 0.2d);
		FACTOR_TO_THROTTLE_MAP.put(THROTTLE_0_4, 0.4d);
		FACTOR_TO_THROTTLE_MAP.put(THROTTLE_0_6, 0.6d);
		FACTOR_TO_THROTTLE_MAP.put(THROTTLE_0_8, 0.8d);
		FACTOR_TO_THROTTLE_MAP.put(THROTTLE_1_0, 1.0d);
	}

	public Analyzer() {
		for (int i = 0; i < THROTTLE_LEVEL_COUNT; i++) {
			speedStats[i] = new DescriptiveStatistics();
			accelStats[i] = new DescriptiveStatistics();
			jerkStats[i] = new DescriptiveStatistics();
			jfStats[i] = new DescriptiveStatistics();
		}
	}
		
	public void addSample(int level, double speed, double accel, double jerk, double jf) {
		if (level == 0 && accel>0) {
			return;
		}
		if (speedStats.length>5000) return;
		speedStats[level].addValue(speed);
		accelStats[level].addValue(accel);
		if (MIN_JERKFACTOR < jf && jf < MAX_JERKFACTOR) { // sanity check to avoid noise values
			jerkStats[level].addValue(jerk);
			jfStats[level].addValue(jf);
		}
	}
	
	// if we start slowing from given speed with zero throttle, what's initial the deceleration?
	public double getInitialDecelerationEstimate(double currentSpeed) {
		double[] speedValues = speedStats[ZERO_THROTTLE].getValues();
		double[] accelValues = accelStats[ZERO_THROTTLE].getValues();
		
		// use commons maths SimpleRegression for linear estimation
		SimpleRegression regression = new SimpleRegression();
		
		// insert data points (speed = x, accel = y)
		for (int i = 0; i < speedValues.length; i++) {
			regression.addData(speedValues[i], accelValues[i]);
		}
		
		return regression.predict(currentSpeed);
	}
	
	public String toString() {
		StringBuffer ret = new StringBuffer();
		for (int i = 0; i < THROTTLE_LEVEL_COUNT; i++) {
			double thr = FACTOR_TO_THROTTLE_MAP.get(i);
			ret.append("THR:" + thr);
			ret.append(" avgspd:" + speedStats[i].getMean() + " ");
			ret.append(" maxspd:" + speedStats[i].getMax() + " ");
			ret.append(" avgacl:" + accelStats[i].getMean() + " ");
			ret.append(" maxacl:" + accelStats[i].getMax() + " ");
			ret.append(" avgjrk:" + jerkStats[i].getMean() + " ");
			ret.append(" maxjrk:" + jerkStats[i].getMax() + " ");
			ret.append(" avgjf:" + jfStats[i].getMean() + " ");
			ret.append(" maxjf:" + jfStats[i].getMax() + " ");
			ret.append("\n");
		}
		return ret.toString();
	}
	

}
